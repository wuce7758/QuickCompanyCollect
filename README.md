###Quick企业信息采集器

运行于java环境的一个免费开源的企业信息采集器（简单的java网络爬虫）。 

目前仅支持赶集网上企业信息的采集，采集内容：公司名称、行业、电话、联系人、地址、公司介绍

信息采集完成后自动导出Excel表格。

基于Jsoup+Poi+Sqlite开发完成。 

详细使用方法介绍地址：[http://www.cntaige.com/archives/83](http://www.cntaige.com/archives/83)